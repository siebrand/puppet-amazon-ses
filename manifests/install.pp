# @summary Installs postfix
#
class amazon_ses::install {

  include ::amazon_ses::params
  package { $::amazon_ses::params::package_names:
      ensure => installed,
  }
}
