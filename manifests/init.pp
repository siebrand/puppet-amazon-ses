# @summary Manages postfix to fuly integrate with Amazon SES.
#
# @param domain
#   The domain of your web site.  In order to send email through SES servers,
#   your domain must be verified.
#   SES Management Console -> Domains -> Verify a New Domain
#   @see http://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-domains.html
#
# @param smtp_username
#   The username of the smtp user.  Note, this is not your IAM user.
#   You need to create a unique user for the SES service.
#   The new user can be created via: SES -> smtp settings -> 'Create My SMTP Credentials' button.
#
# @param smtp_password
#   The password of the smtp user.
#
# @param inet_interfaces
#   The interfaces to which postfix should bind
#
# @param inet_protocols
#   The protocols postfix should use. 'ipv4', 'ipv6', 'all'.
#
# @param ses_region
#  The region of the Amazon smtp server to relay to.
#  Amazon only offers [3 regions](http://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html) with 1 availability zone each.
#
#   * 'US EAST'      - N. Virginia (us-east-1)
#   * 'US WEST'      - Oregon (us-west-2)
#   * 'EU'           - Ireland (eu-west-1)
#   * 'us-east-1'    - uses 'US EAST'
#   * 'us-west-1'    - uses 'US WEST'
#   * 'us-west-2'    - uses 'US WEST'
#   * 'eu-west-1'    - uses 'EU'
#   * 'eu-central-1' - uses 'EU'
#
# @param smtp_port
#   The port used to connect to the Amazon SMTP server.
#   The default is 587 as there are no limits. If you use port 25, than you will
#   need to request that Amazon disables the rate limit (which is 1 email per minute).
#
# @param smtp_tls_ca_file
#   A file containing CA certificates of root CAs trusted to sign either
#   remote SMTP server certificates or intermediate CA certificates.
#   If not specified the OS default location is used.
#
# @param smtpd_tls_cert_file
#   File with the Postfix SMTP server RSA certificate in PEM format. This
#   file may also contain the Postfix SMTP server pri vate RSA key.
#   If not specified the OS default location is used.
#
# @param smtpd_tls_key_file
#   File with the Postfix SMTP server RSA private key in PEM format. This
#   file may be combined with the Postfix SMTP server RSA certificate file
#   specified with $smtpd_tls_cert_file. The private key must be accessible
#   without a pass-phrase, i.e. it must not be encrypted.
#   If not specified the OS default location is used.
#
# @param mynetworks
#   The mynetworks setting to use for postfix.
#
# @param message_size_limit
#   The maximum size in bytes of a message including envelope information.
#
# @param smtpd_relay_restrictions
#   Access restrictions for mail relay control.
#   @see http://www.postfix.org/postconf.5.html#smtpd_relay_restrictions
#
# @example
#
#   # Sets up SES for the US EAST region
#   class { 'amazon_ses':
#     domain        => 'test.com',
#     smtp_username => 'USERNAME',
#     smtp_password => 'PASSWORD',
#   }
#
#   # Sets up SES for the EU region
#   class { 'amazon_ses':
#     domain        => 'test.com',
#     smtp_username => 'USERNAME',
#     smtp_password => 'PASSWORD',
#     ses_region    => 'EU',
#   }
#
class amazon_ses (
  String           $domain,
  String           $smtp_username,
  String           $smtp_password,
  String           $inet_interfaces          = $::amazon_ses::params::default_inet_interfaces,
  String           $inet_protocols           = $::amazon_ses::params::default_inet_protocols,
  String           $ses_region               = $::amazon_ses::params::default_ses_region,
  Integer          $smtp_port                = $::amazon_ses::params::default_smtp_port,
  String           $smtp_tls_ca_file         = $::amazon_ses::params::default_smtp_tls_ca_file,
  String           $smtpd_tls_cert_file      = $::amazon_ses::params::default_smtpd_tls_cert_file,
  String           $smtpd_tls_key_file       = $::amazon_ses::params::default_smtpd_tls_key_file,
  String           $mynetworks               = $::amazon_ses::params::default_mynetworks,
  Integer          $message_size_limit       = $::amazon_ses::params::default_message_size_limit,
  Optional[String] $smtpd_relay_restrictions = undef,
  Boolean          $virtual_root             = false,
) inherits ::amazon_ses::params {

  # check for OS Family
  case $facts['os']['family'] {
    'debian', 'fedora', 'redhat': {
      # debian (so ubuntu and debian are only supported)
      # we also support RedHat/CentOS and Fedora
    }
    default: {
      fail("${facts['os']['family']} - Unsupported OS Family")
    }
  }


  # Maps the real AWS regions to the SES service regions
  # Previously-used keys added for backwards compatibility
  $ses_region_map = {
    'us-east-1'    => 'us_east',
    'us-west-1'    => 'us_west',
    'us-west-2'    => 'us_west',
    'eu-west-1'    => 'eu',
    'eu-central-1' => 'eu',
    'eu-north-1'   => 'eu_north_1',
    'US EAST'      => 'us_east',
    'US WEST'      => 'us_west',
    'EU'           => 'eu',
  }

  # $region_url
  #   The full url based on the region for the SES smtp server

  # Data for SES service by region
  $ses_data = {
    us_east => {
      region_url => "email-smtp.us-east-1.amazonaws.com:${smtp_port}",
    },
    us_west => {
      region_url => "email-smtp.us-west-2.amazonaws.com:${smtp_port}",
    },
    eu => {
      region_url => "email-smtp.eu-west-1.amazonaws.com:${smtp_port}",
    },
    eu_north_1 => {
      region_url => "email-smtp.eu-north-1.amazonaws.com:${smtp_port}",
    },
  }

  if $ses_region_map[$ses_region] {
    $region_url = $ses_data[$ses_region_map[$ses_region]][region_url]
  }
  else {
    fail("${ses_region} - Invalid ses_region")
  }

  contain amazon_ses::install
  contain amazon_ses::config
  contain amazon_ses::service

  Class['amazon_ses::install'] -> Class['amazon_ses::config'] -> Class['amazon_ses::service']

  if $virtual_root {
    file { '/etc/postfix/virtual':
      ensure  => present,
      content => 'root root@localhost',
      require => Package['postfix']
    }

    exec { 'rebuild.virtual.db':
      command     => '/sbin/postmap /etc/postfix/virtual',
      subscribe   => File['/etc/postfix/virtual'],
      refreshonly => true,
      notify      => Service['postfix']
    }
  }
}
