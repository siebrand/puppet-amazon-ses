# @summary Manages the configuration files for postfix.
#
class amazon_ses::config {

  # manage the postfix main configuration file 
  file {'/etc/postfix/main.cf':
    ensure  => file,
    content => epp('amazon_ses/main.cf.epp',{
      hostname                 => $facts['networking']['hostname'],
      smtpd_tls_cert_file      => $amazon_ses::smtpd_tls_cert_file,
      smtpd_tls_key_file       => $amazon_ses::smtpd_tls_key_file,
      smtp_tls_ca_file         => $amazon_ses::smtp_tls_ca_file,
      mynetworks               => $amazon_ses::mynetworks,
      inet_interfaces          => $amazon_ses::inet_interfaces,
      domain                   => $amazon_ses::domain,
      inet_protocols           => $amazon_ses::inet_protocols,
      message_size_limit       => $amazon_ses::message_size_limit,
      region_url               => $amazon_ses::region_url,
      smtpd_relay_restrictions => $amazon_ses::smtpd_relay_restrictions,
      virtual_root             => $amazon_ses::virtual_root,
    }),
  }

  # setup passwords
  # note, its best practice to delete this file
  # however, it needs to exist in order for puppet
  # to work correctly with generating the db file
  # also, if the password changes, creating the db file is notified.
  # So the solution for now is to restrict the permissions.
  file { '/etc/postfix/sasl_passwd':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
    content => epp('amazon_ses/sasl_passwd.epp',{
      region_url    => $amazon_ses::region_url,
      smtp_username => $amazon_ses::smtp_username,
      smtp_password => $amazon_ses::smtp_password,
    })
  }

  # recreate the db if either file changes
  exec { 'create_db_file':
    command     => '/usr/sbin/postmap -r hash:/etc/postfix/sasl_passwd',
    cwd         => '/etc/postfix',
    refreshonly => true,
    subscribe   => [
      File['/etc/postfix/sasl_passwd'],
      File['/etc/postfix/main.cf'],
    ],
  }
}
