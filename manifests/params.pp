# @summary Sets OS-specific values
#
class amazon_ses::params{

  $default_inet_interfaces = 'all'

  $default_inet_protocols = 'all'

  $default_ses_region = 'US EAST'

  $default_smtp_port = 587

  $default_smtp_tls_ca_file = $facts['os']['family'] ? {
    'debian' => '/etc/ssl/certs/ca-certificates.crt',
    default  => '/etc/pki/tls/cert.pem',
  }

  $default_smtpd_tls_cert_file = $facts['os']['family'] ? {
    'debian' => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
    default  => '/etc/pki/tls/certs/localhost.crt',
  }

  $default_smtpd_tls_key_file = $facts['os']['family'] ? {
    'debian' => '/etc/ssl/private/ssl-cert-snakeoil.key',
    default  => '/etc/pki/tls/private/localhost.key',
  }

  $package_names = $facts['os']['family'] ? {
    'debian' => ['postfix', 'libsasl2-modules'],
    default  => ['postfix', 'cyrus-sasl-plain'],
  }

  $default_mynetworks = '127.0.0.0/8 [::ffff:127.0.0.0]/104 [::1]/128'

  $default_message_size_limit = 10240000
}
